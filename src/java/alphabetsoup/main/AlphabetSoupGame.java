package alphabetsoup.main;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import alphabetsoup.service.GameService;


public class AlphabetSoupGame {

    private static final String inputFileLocation = "C:/ABC123/testfile.txt";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss");


    public static void main(String[] args) {
        AlphabetSoupGame.playGame();
    }


    private static void playGame() {
        System.out.println(LocalDateTime.now().format(formatter));
        try {
            GameService service = new GameService();
            service.playAlphabetSoupGame(inputFileLocation);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        System.out.println(LocalDateTime.now().format(formatter));
    }

}
