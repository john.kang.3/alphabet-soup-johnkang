package alphabetsoup.util;

import java.util.ArrayList;
import java.util.List;

import alphabetsoup.domain.GameDtoOuterClass.CharLocationCoordinatesDto;
import alphabetsoup.domain.GameDtoOuterClass.LocatedTargetWordDto;
import alphabetsoup.domain.GameDtoOuterClass.LocatedTargetWordDto.LineDirectionEnum;
import alphabetsoup.domain.GameDtoOuterClass.LocatedTargetWordDto.LineTypeEnum;


public class GameUtils {

    public static List<CharLocationCoordinatesDto> locateCoordinatesOfTargetChar(char targetChar, char[][] matrix) {
        List<CharLocationCoordinatesDto> dtoList = new ArrayList<>();

        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                if (targetChar == matrix[row][column]) {
                    dtoList.add(new CharLocationCoordinatesDto().setIsCharLocated(true).setRowCoordinate(row).setColumnCoordinate(column));
                    continue;
                }
            }
        }
        return dtoList;
    }



    private enum TargetWordTypeEnum {
        FORWARDS_HORIZONTALLY_FROM_LEFT_TO_RIGHT,
        FORWARDS_VERTICALLY_FROM_TOP_TO_BOTTOM,
        FORWARDS_DIAGONALLY_FROM_TOP_LEFT_TO_BOTTOM_RIGHT,
        FORWARDS_DIAGONALLY_FROM_BOTTOM_LEFT_TO_TOP_RIGHT,
        BACKWARDS_HORIZONTALLY_FROM_RIGHT_TO_LEFT,
        BACKWARDS_VERTICALLY_FROM_BOTTOM_TO_TOP,
        BACKWARDS_DIAGONALLY_FROM_TOP_RIGHT_TO_BOTTOM_LEFT,
        BACKWARDS_DIAGONALLY_FROM_BOTTOM_RIGHT_TO_TOP_LEFT;
    }


    public static List<LocatedTargetWordDto> locateTargetWordInMatrix(String targetWord, char[][] matrix, CharLocationCoordinatesDto dto) {
        List<LocatedTargetWordDto> locatedTargetWordList = new ArrayList<>();

        for (TargetWordTypeEnum typeEnum : TargetWordTypeEnum.values()) {
            LocatedTargetWordDto candidateDto = performSearch(typeEnum, targetWord, matrix, dto);
            if (candidateDto != null) {
                locatedTargetWordList.add(candidateDto);
            }
        }
        return locatedTargetWordList;
    }


    private static LocatedTargetWordDto performSearch(
            TargetWordTypeEnum targetWordType, String targetWord, char[][] matrix, CharLocationCoordinatesDto dto) {

        if (reduceCodeClutter_1(targetWordType, targetWord, matrix, dto)) {
            return null;
        }

        int X = dto.getRowCoordinate();
        int Y = dto.getColumnCoordinate();
        StringBuffer testWord = new StringBuffer().append(matrix[X][Y]);

        LocatedTargetWordDto locatedWord = 
                reduceCodeClutter_2(targetWordType).setLocatedWord(targetWord).setRowStartCoordinate(X).setColumnStartCoordinate(Y);

        int counter = 0;
        while (++counter < targetWord.length()) {
            reduceCodeClutter_3(targetWordType, testWord, locatedWord, matrix, X, Y, counter);
            if (! targetWord.startsWith(testWord.toString())) {
                return null;
            }
        }

        if (targetWord.equals(testWord.toString())) {
            return locatedWord;
        }
        return null;
    }


    private static boolean reduceCodeClutter_1(
            TargetWordTypeEnum targetWordType, String targetWord, char[][] matrix, CharLocationCoordinatesDto dto) {

        boolean YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX = true;

        switch (targetWordType) {
            case FORWARDS_HORIZONTALLY_FROM_LEFT_TO_RIGHT:
                    if (willTargetWordBeOutsideOfMatrixBoundary(dto.getColumnCoordinate() + targetWord.length(), matrix[0].length)) {
                        return YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX;
                    }
                    return false;
            case FORWARDS_VERTICALLY_FROM_TOP_TO_BOTTOM:
                    if (willTargetWordBeOutsideOfMatrixBoundary(dto.getRowCoordinate() + targetWord.length(), matrix.length)) {
                        return YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX;
                    }
                    return false;
            case FORWARDS_DIAGONALLY_FROM_TOP_LEFT_TO_BOTTOM_RIGHT:
                    if (willTargetWordBeOutsideOfMatrixBoundary(dto.getColumnCoordinate() + targetWord.length(), matrix[0].length)
                            || willTargetWordBeOutsideOfMatrixBoundary(dto.getRowCoordinate() + targetWord.length(), matrix.length)) {
                        return YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX;
                    }
                    return false;
            case FORWARDS_DIAGONALLY_FROM_BOTTOM_LEFT_TO_TOP_RIGHT:
                    if (willTargetWordBeOutsideOfMatrixBoundary(dto.getColumnCoordinate() + targetWord.length(), matrix[0].length)
                            || willTargetWordBeOutsideOfMatrixBoundary(dto.getRowCoordinate() - targetWord.length(), -1, true)) {
                        return YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX;
                    }
                    return false;
            case BACKWARDS_HORIZONTALLY_FROM_RIGHT_TO_LEFT:
                    if (willTargetWordBeOutsideOfMatrixBoundary(dto.getColumnCoordinate() - targetWord.length(), -1, true)) {
                        return YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX;
                    }
                    return false;
            case BACKWARDS_VERTICALLY_FROM_BOTTOM_TO_TOP:
                    if (willTargetWordBeOutsideOfMatrixBoundary(dto.getRowCoordinate() - targetWord.length(), -1, true)) {
                        return YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX;
                    }
                    return false;
            case BACKWARDS_DIAGONALLY_FROM_TOP_RIGHT_TO_BOTTOM_LEFT:
                    if (willTargetWordBeOutsideOfMatrixBoundary(dto.getColumnCoordinate() - targetWord.length(), -1, true)
                            || willTargetWordBeOutsideOfMatrixBoundary(dto.getRowCoordinate() + targetWord.length(), matrix.length)) {
                        return YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX;
                    }
                    return false;
            case BACKWARDS_DIAGONALLY_FROM_BOTTOM_RIGHT_TO_TOP_LEFT:
                    if (willTargetWordBeOutsideOfMatrixBoundary(dto.getColumnCoordinate() - targetWord.length(), -1, true)
                            || willTargetWordBeOutsideOfMatrixBoundary(dto.getRowCoordinate() - targetWord.length(), -1, true)) {
                        return YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX;
                    }
                    return false;
            default:
                    return YES_TARGET_WORD_WILL_BE_PARTIALLY_OUTSIDE_MATRIX;
        }
    }
    private static LocatedTargetWordDto reduceCodeClutter_2(TargetWordTypeEnum targetWordType) {
        switch (targetWordType) {
            case FORWARDS_HORIZONTALLY_FROM_LEFT_TO_RIGHT:
                    return new LocatedTargetWordDto().setLineType(LineTypeEnum.FORWARD).setLineDirection(LineDirectionEnum.HORIZONTAL);
            case FORWARDS_VERTICALLY_FROM_TOP_TO_BOTTOM:
                    return new LocatedTargetWordDto().setLineType(LineTypeEnum.FORWARD).setLineDirection(LineDirectionEnum.VERTICAL);
            case FORWARDS_DIAGONALLY_FROM_TOP_LEFT_TO_BOTTOM_RIGHT:
                    return new LocatedTargetWordDto().setLineType(LineTypeEnum.FORWARD).setLineDirection(LineDirectionEnum.DIAGONAL_TOP_TO_BOTTOM);
            case FORWARDS_DIAGONALLY_FROM_BOTTOM_LEFT_TO_TOP_RIGHT:
                    return new LocatedTargetWordDto().setLineType(LineTypeEnum.FORWARD).setLineDirection(LineDirectionEnum.DIAGONAL_BOTTOM_TO_TOP);
            case BACKWARDS_HORIZONTALLY_FROM_RIGHT_TO_LEFT:
                    return new LocatedTargetWordDto().setLineType(LineTypeEnum.BACKWARD).setLineDirection(LineDirectionEnum.HORIZONTAL);
            case BACKWARDS_VERTICALLY_FROM_BOTTOM_TO_TOP:
                    return new LocatedTargetWordDto().setLineType(LineTypeEnum.BACKWARD).setLineDirection(LineDirectionEnum.VERTICAL);
            case BACKWARDS_DIAGONALLY_FROM_TOP_RIGHT_TO_BOTTOM_LEFT:
                    return new LocatedTargetWordDto().setLineType(LineTypeEnum.BACKWARD).setLineDirection(LineDirectionEnum.DIAGONAL_TOP_TO_BOTTOM);
            case BACKWARDS_DIAGONALLY_FROM_BOTTOM_RIGHT_TO_TOP_LEFT:
                    return new LocatedTargetWordDto().setLineType(LineTypeEnum.BACKWARD).setLineDirection(LineDirectionEnum.DIAGONAL_BOTTOM_TO_TOP);
            default:
                    return new LocatedTargetWordDto();
        }
    }
    private static void reduceCodeClutter_3(
            TargetWordTypeEnum targetWordType, StringBuffer testWord, LocatedTargetWordDto locatedWord, char[][] matrix, int X, int Y, int counter) {

        switch (targetWordType) {
            case FORWARDS_HORIZONTALLY_FROM_LEFT_TO_RIGHT:
                    testWord.append(matrix[X][Y + counter]);
                    locatedWord.setRowEndCoordinate(X).setColumnEndCoordinate(Y + counter);
                    break;
            case FORWARDS_VERTICALLY_FROM_TOP_TO_BOTTOM:
                    testWord.append(matrix[X + counter][Y]);
                    locatedWord.setRowEndCoordinate(X + counter).setColumnEndCoordinate(Y);
                    break;
            case FORWARDS_DIAGONALLY_FROM_TOP_LEFT_TO_BOTTOM_RIGHT:
                    testWord.append(matrix[X + counter][Y + counter]);
                    locatedWord.setRowEndCoordinate(X + counter).setColumnEndCoordinate(Y + counter);
                    break;
            case FORWARDS_DIAGONALLY_FROM_BOTTOM_LEFT_TO_TOP_RIGHT:
                    testWord.append(matrix[X - counter][Y + counter]);
                    locatedWord.setRowEndCoordinate(X - counter).setColumnEndCoordinate(Y + counter);
                    break;
            case BACKWARDS_HORIZONTALLY_FROM_RIGHT_TO_LEFT:
                    testWord.append(matrix[X][Y - counter]);
                    locatedWord.setRowEndCoordinate(X).setColumnEndCoordinate(Y - counter);
                    break;
            case BACKWARDS_VERTICALLY_FROM_BOTTOM_TO_TOP:
                    testWord.append(matrix[X - counter][Y]);
                    locatedWord.setRowEndCoordinate(X - counter).setColumnEndCoordinate(Y);
                    break;
            case BACKWARDS_DIAGONALLY_FROM_TOP_RIGHT_TO_BOTTOM_LEFT:
                    testWord.append(matrix[X + counter][Y - counter]);
                    locatedWord.setRowEndCoordinate(X + counter).setColumnEndCoordinate(Y - counter);
                    break;
            case BACKWARDS_DIAGONALLY_FROM_BOTTOM_RIGHT_TO_TOP_LEFT:
                    testWord.append(matrix[X - counter][Y - counter]);
                    locatedWord.setRowEndCoordinate(X - counter).setColumnEndCoordinate(Y - counter);
                    break;
        }
    }


    private static boolean willTargetWordBeOutsideOfMatrixBoundary(int testBoundary, int actualBoundary) {
        return willTargetWordBeOutsideOfMatrixBoundary(testBoundary, actualBoundary, false);
    }

    private static boolean willTargetWordBeOutsideOfMatrixBoundary(int testBoundary, int actualBoundary, boolean isInverted) {
        if (isInverted) {
            if (testBoundary < actualBoundary) {
                return true;
            }
            return false;
        }
        else {
            if (testBoundary > actualBoundary) {
                return true;
            }
            return false;
        }
    }

}
