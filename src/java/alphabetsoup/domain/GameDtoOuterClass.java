package alphabetsoup.domain;


public class GameDtoOuterClass {

    public static class LocatedTargetWordDto {

        String locatedWord;
        public String getLocatedWord() { return locatedWord; }
        public LocatedTargetWordDto setLocatedWord(String pVal) { locatedWord = pVal;  return this; }

        LineTypeEnum lineType;
        public LineTypeEnum getLineType() { return lineType; }
        public LocatedTargetWordDto setLineType(LineTypeEnum pVal) { lineType = pVal;  return this; }

        LineDirectionEnum lineDirection;
        public LineDirectionEnum getLineDirection() { return lineDirection; }
        public LocatedTargetWordDto setLineDirection(LineDirectionEnum pVal) { lineDirection = pVal;  return this; }

        int rowStartCoordinate = -1;
        public int getRowStartCoordinate() { return rowStartCoordinate; }
        public LocatedTargetWordDto setRowStartCoordinate(int pVal) { rowStartCoordinate = pVal;  return this; }

        int columnStartCoordinate = -1;
        public int getColumnStartCoordinate() { return columnStartCoordinate; }
        public LocatedTargetWordDto setColumnStartCoordinate(int pVal) { columnStartCoordinate = pVal;  return this; }

        int columnEndCoordinate = -1;
        public int getColumnEndCoordinate() { return columnEndCoordinate; }
        public LocatedTargetWordDto setColumnEndCoordinate(int pVal) { columnEndCoordinate = pVal;  return this; }

        int rowEndCoordinate = -1;
        public int getRowEndtCoordinate() { return rowEndCoordinate; }
        public LocatedTargetWordDto setRowEndCoordinate(int pVal) { rowEndCoordinate = pVal;  return this; }


        @Override
        public String toString() {
            return locatedWord + " " + rowStartCoordinate + ":" + columnStartCoordinate + " " + rowEndCoordinate + ":" + columnEndCoordinate;
        }

        public enum LineTypeEnum {
            FORWARD, BACKWARD;
        }

        public enum LineDirectionEnum {
            HORIZONTAL, VERTICAL, DIAGONAL_TOP_TO_BOTTOM, DIAGONAL_BOTTOM_TO_TOP;
        }

    }



    public static class CharLocationCoordinatesDto {

        boolean isCharLocated = false;
        public boolean getIsCharLocated() { return isCharLocated; }
        public CharLocationCoordinatesDto setIsCharLocated(boolean pVal) { isCharLocated = pVal;  return this; }

        int rowCoordinate = -1;
        public int getRowCoordinate() { return rowCoordinate; }
        public CharLocationCoordinatesDto setRowCoordinate(int pVal) { rowCoordinate = pVal;  return this; }

        int columnCoordinate = -1;
        public int getColumnCoordinate() { return columnCoordinate; }
        public CharLocationCoordinatesDto setColumnCoordinate(int pVal) { columnCoordinate = pVal;  return this; }

    }

}
