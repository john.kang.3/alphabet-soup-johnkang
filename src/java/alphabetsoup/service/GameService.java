package alphabetsoup.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;

import alphabetsoup.dataaccess.GameDao;
import alphabetsoup.domain.GameDtoOuterClass.CharLocationCoordinatesDto;
import alphabetsoup.domain.GameDtoOuterClass.LocatedTargetWordDto;
import alphabetsoup.util.GameUtils;


public class GameService {

    /* Write self-documenting code, as code comprehension and code maintenance will be/remain more efficient.
     * And avoid un-necessary comments, that will inevitably become outdated, and lead to confusion regarding business rules.
     */



    public void playAlphabetSoupGame(String inputFileLocation) throws Exception {
        char[][] twoDimensionalCharArray = determine2DArrayConstraintsAndReturn2DArray(inputFileLocation);

        parseInputDataAndUseToPopulate2DArray(twoDimensionalCharArray, inputFileLocation, twoDimensionalCharArray.length);

        List<String> allTargetWords = 
                new GameDao().parseInputFileByStartAndStopConstraints(inputFileLocation, twoDimensionalCharArray.length + 1, Optional.empty());

        LinkedHashSet<String> targetWordsToFindIn2DArray = removeWhitespaceAndFilterDuplicateTargetWords(allTargetWords);

        List<LocatedTargetWordDto> locatedTargetWordsList = new ArrayList<>();
        for (String targetWord : targetWordsToFindIn2DArray) {
            locatedTargetWordsList.addAll(findTargetWordIn2DArray(targetWord, twoDimensionalCharArray));
        }
        //targetWordsToFindIn2DArray.forEach(targetWord -> locatedTargetWordsList.addAll(findTargetWordIn2DArray(targetWord, twoDimensionalCharArray)));

        for (LocatedTargetWordDto locatedWordDto : locatedTargetWordsList) {
            System.out.println(locatedWordDto.toString());
        }
    }


    private char[][] determine2DArrayConstraintsAndReturn2DArray(String inputFileLocation) throws Exception {
        String arrayBoundariesAsString = new GameDao().readAndReturnFirstLine(inputFileLocation);
        List<String> arrayBoundaries = Arrays.asList(arrayBoundariesAsString.split("x"));

        System.out.println(arrayBoundariesAsString);
        return new char[Integer.valueOf(arrayBoundaries.get(0))][Integer.valueOf(arrayBoundaries.get(1))];
    }


    private void parseInputDataAndUseToPopulate2DArray(char[][] twoDimensionalCharArray, String inputFileLocation, Integer arrayLength) throws Exception {
        List<String> spaceDelimListOfStrings = new GameDao().parseInputFileByStartAndStopConstraints(inputFileLocation, 1, Optional.of(arrayLength + 1));

        int counter = 0;
        for (String spaceDelimString : spaceDelimListOfStrings) {
            twoDimensionalCharArray[counter++] = spaceDelimString.replace(" ", "").toCharArray();
        }
    }


    /* Execution of this method is necessary, because "..words that have spaces in them will not include spaces when hidden in the grid of characters"
     * In other words, target words from the input file may include whitespace characters, however the 2D array (matrix) will not whitespace char-values.
     */
    private LinkedHashSet<String> removeWhitespaceAndFilterDuplicateTargetWords(List<String> allTargetWords) throws Exception {
        LinkedHashSet<String> retInsertionOrderedSet = new LinkedHashSet<>();

        for (String targetWord: allTargetWords) {
            retInsertionOrderedSet.add(targetWord.replace(" ", ""));
        }
        return retInsertionOrderedSet;
    }


    private List<LocatedTargetWordDto> findTargetWordIn2DArray(String targetWord, char[][] twoDimensionalCharArray) throws Exception {
        List<CharLocationCoordinatesDto> dtoList = GameUtils.locateCoordinatesOfTargetChar(targetWord.charAt(0), twoDimensionalCharArray);

        List<LocatedTargetWordDto> locatedTargetWordList = new ArrayList<>();
        for (CharLocationCoordinatesDto dto : dtoList) {
            locatedTargetWordList.addAll(GameUtils.locateTargetWordInMatrix(targetWord, twoDimensionalCharArray, dto));
        }
        return locatedTargetWordList;
    }

}
