package alphabetsoup.dataaccess;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;


public class GameDao {

    /* Data read/write classes should:
     * 1) implement re-usable, generic CRUD operations.
     * 2) perform CRUD operations and only CRUD, avoiding business rules (at all costs).
     */



    public String readAndReturnFirstLine(String inputFileLocation) throws Exception {
        try (Scanner scanner = new Scanner(new File(inputFileLocation))) {
            //return scanner.useDelimiter("\\n").next().trim();   // ...useDelimiter("\\Z");
            return scanner.nextLine();
        }
    }


    public List<String> parseInputFileByStartAndStopConstraints(String inputFileLocation, Integer startLine, Optional<Integer> stopLine) throws Exception {
        List<String> retList = new ArrayList<>();

        try (Scanner scanner = new Scanner(new File(inputFileLocation))) {
            int lineCounter = 0;
            while (scanner.hasNextLine()) {
                lineCounter++;

                if (lineCounter <= startLine) {
                    scanner.nextLine();
                    continue;
                }
                if (stopLine.isPresent() && lineCounter > stopLine.get()) {
                    break;
                }
                retList.add(scanner.nextLine());
            }
        }
        return retList;
    }

}
